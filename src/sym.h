#ifndef __BLOP_SYM_H__
#define __BLOP_SYM_H__

/*
All the symbolic constants are implemented as an enumerated type
within the blop::sym namespace, in order to avoid global
namespace pollution
 */

namespace blop
{
  namespace sym
  {
      typedef enum { begin=1, end, center, left, right, top, bottom, base, up, down } position;
      typedef enum { solid=100, dotted, dashed, dashdotted, dashddotted } linestyle;
      typedef enum { right_down, right_up, left_down, left_up, down_right, down_left, up_right, up_left } scan2d;
      typedef enum { horizontal=1, vertical=2 } horizontal_vertical;
  }
}

#endif
