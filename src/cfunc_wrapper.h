#ifndef __BLOP_CFUNC_WRAPPER_H__
#define __BLOP_CFUNC_WRAPPER_H__

#include "var.h"
#include <complex>

namespace blop
{
    extern var     temporary_result_var;
    extern std::complex<double> temporary_result_complex;
}

#endif
