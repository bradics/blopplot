#include "set.h"

namespace blop
{
    tic_setter set::xtics(axis::x1);
    tic_setter set::ytics(axis::y1);
    tic_setter set::x1tics(axis::x1);
    tic_setter set::x2tics(axis::x2);
    tic_setter set::y1tics(axis::y1);
    tic_setter set::y2tics(axis::y2);

    replot_request set::y2transform(const function &to, const function &back, bool own_tics)
    {
	frame::current().y2axis()->transform(frame::current().y1axis(), to, back, own_tics);
	return replot_request();
    }
    replot_request set::x2transform(const function &to, const function &back, bool own_tics)
    {
	frame::current().x2axis()->transform(frame::current().x1axis(), to, back, own_tics);
	return replot_request();
    }
    replot_request set::y1transform(const function &to, const function &back, bool own_tics)
    {
	frame::current().y1axis()->transform(frame::current().y2axis(), to, back, own_tics);
	return replot_request();
    }
    replot_request set::x1transform(const function &to, const function &back, bool own_tics)
    {
	frame::current().x1axis()->transform(frame::current().x2axis(), to, back, own_tics);
	return replot_request();
    }
}
